package com.test.testappodeal

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.appodeal.ads.Appodeal
import com.appodeal.ads.AppodealNetworks
import com.appodeal.ads.BannerCallbacks

class MainActivity : AppCompatActivity() {


    private val APPODEAL_KEY = "21090f017b8bdd9f6e9f4f760e816dc305636334b2ee02a7"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
// disable another network
//        Appodeal.disableNetwork(this, AppodealNetworks.VUNGLE)
//        Appodeal.disableNetwork(this, AppodealNetworks.ADCOLONY)
//        Appodeal.disableNetwork(this, AppodealNetworks.VPAID)
//        Appodeal.disableNetwork(this, AppodealNetworks.APPNEXUS)
//        Appodeal.disableNetwork(this, AppodealNetworks.CHARTBOOST)
//        Appodeal.disableNetwork(this, AppodealNetworks.SMAATO)
//        Appodeal.disableNetwork(this, AppodealNetworks.UNITY_ADS)
//        Appodeal.disableNetwork(this, AppodealNetworks.STARTAPP)
//        Appodeal.disableNetwork(this, AppodealNetworks.IRON_SOURCE)
//        Appodeal.disableNetwork(this, AppodealNetworks.TAPJOY)
//        Appodeal.disableNetwork(this, AppodealNetworks.YANDEX)
//        Appodeal.disableNetwork(this, AppodealNetworks.APPODEAL)
//        Appodeal.disableNetwork(this, AppodealNetworks.APPODEALX)
//        Appodeal.disableNetwork(this, AppodealNetworks.APPLOVIN)
//        Appodeal.disableNetwork(this, AppodealNetworks.INMOBI)
//        Appodeal.disableNetwork(this, AppodealNetworks.MOPUB)
//        Appodeal.disableNetwork(this, AppodealNetworks.MINTEGRAL)
//        Appodeal.disableNetwork(this, AppodealNetworks.FACEBOOK)

        //
//        Appodeal.disableNetwork(this, AppodealNetworks.MY_TARGET)

        Appodeal.initialize(
            this,
            APPODEAL_KEY,
            Appodeal.BANNER or Appodeal.INTERSTITIAL or Appodeal.NATIVE
            , true
        )
        Appodeal.setBannerViewId(R.id.appodealBannerView)
        Appodeal.setBannerCallbacks(BannerListener())
    }

    override fun onResume() {
        super.onResume()
        Appodeal.show(this, Appodeal.BANNER_VIEW)
    }

    override fun onStop() {
        super.onStop()
        Appodeal.destroy(Appodeal.BANNER_VIEW)
    }

    private inner class BannerListener : BannerCallbacks {
        override fun onBannerExpired() {

        }

        override fun onBannerShown() {

        }

        override fun onBannerLoaded(p0: Int, p1: Boolean) {

        }

        override fun onBannerClicked() {

        }

        override fun onBannerFailedToLoad() {
        }

    }

}
